class Node:
    def __init__(self, val):
        self.l = None
        self.r = None
        self.v = val

class Tree:
    def __init__(self):
        self.root = None

    def add(self, val):
        if self.root is None:
            self.root = Node(val)
        else:
            self._add(val, self.root)

    def _add(self, val, node):
        if val < node.v:
            if node.l is not None:
                self._add(val, node.l)
            else:
                node.l = Node(val)
        else:
            if node.r is not None:
                self._add(val, node.r)
            else:
                node.r = Node(val)

    def find(self, val):
        if self.root is not None:
            return self._find(val, self.root)
        else:
            return None

    def _find(self, val, node):
        if val == node.v:
            return node
        elif (val < node.v and node.l is not None):
            return self._find(val, node.l)
        elif (val > node.v and node.r is not None):
            return self._find(val, node.r)


    def lca(self, v1, v2):
        if self.find(v1) and self.find(v2):
            return self._lca(self.root, v1, v2)
        else:
            return None

    def _lca(self, node, v1, v2):
        if node.v is not None and  v1 < node.v and v2 < node.v:
            return self._lca(node.l, v1, v2)
        elif node.v is not None and v1 > node.v and v2 > node.v:
            return self._lca(node.r, v1, v2)
        else:
            return node