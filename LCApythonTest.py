import unittest
from src import*


class TestLCApython(unittest.TestCase):
    def testContains(self):
        bst = Tree()
        bst.add(7)   #        _7_
        bst.add(8)   #      /     \
        bst.add(3)   #    _3_      8
        bst.add(1)   #  /     \
        bst.add(2)   # 1       6
        bst.add(6)   #  \     /
        bst.add(4)   #   2   4
        bst.add(5)   #        \

        self.assertTrue(bst.find(5),"Contains valid key")
        self.assertFalse(bst.find(9),"Contains invalid key")

    def testLCA(self):
        bst = Tree()
        bst.add(7)   #        _7_
        bst.add(8)   #      /     \
        bst.add(3)   #    _3_      8
        bst.add(1)   #  /     \
        bst.add(2)   # 1       6
        bst.add(6)   #  \     /
        bst.add(4)   #   2   4
        bst.add(5)   #        \

        self.assertIsNone(bst.lca(25, 0), "Invalid test Keys");
        self.assertIsNone(bst.lca(2, 25), "One invalid, one valid");
        self.assertEqual(bst.lca(2, 8).v, 7, "Valid test keys - root is LCA", );
        self.assertEqual(bst.lca(2, 4).v, 3, "Valid test keys - something else is LCA",);
        self.assertEqual(bst.lca(4, 5).v, 4, "Valid Test Keys - one more for good measure");
      
if __name__ == '__main__':
    unittest.main()
